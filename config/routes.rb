Rails.application.routes.draw do
  post '/adicionar', to: 'celulas#add_to_celula', as: :add_to_cell
  root 'metas_gerais#index'
  put '/troca', to: 'tasks#change_status', as: :task_change_status
  resources :projects
  resources :technologies
  resources :tags
  resources :articles
  mount Ckeditor::Engine => '/ckeditor'
  resources :tasks
  resources :goals
  resources :celulas
  resources :posts
  devise_for :users, :controllers => {registrations: 'registration'}, skip: [:sessions]
  as :user do
    get '/login', to: 'devise/sessions#new', as: :new_user_session
    post '/login', to: 'devise/sessions#create', as: :user_session
    delete '/logout', to: 'devise/sessions#destroy', as: :destroy_user_session
  end
  resources :users
  resources :offices
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :posts do 
    member do 
      put 'like' => "posts#like"
      put 'dislike' => "posts#dislike"
    end
  end 
end
