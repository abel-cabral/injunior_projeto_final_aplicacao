# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# Foram criados 10 usuários, todos com senha 123456. Os emails são sampleN@injunior.com.br, onde N é um numero de 0 a 9.
# Foi criado um usuario admin. O email é admin@injunior.com.br, a senha também é 123456.
# Foram criadas diretorias e células sem gerentes, diretores ou membros, para que possa ser feito o teste.

def lang
    Tag.create(name: Faker::ProgrammingLanguage.name)
end


50.times do |i|
    lang()
end

User.create(name: "admin", email: "admin@injunior.com.br", password: "123456", password_confirmation: "123456", admin: true)
10.times do |i|
    User.create(name: Faker::FunnyName.name, email: "sample"+i.to_s+"@injunior.com.br", password:"123456", password_confirmation: "123456")
end

Office.create(name: "Gestão de Pessoas")
Office.create(name: "Projetos")
Office.create(name: "Presidência")
Office.create(name: "Financeiro")
Office.create(name: "Marketing")

Celula.create(name: "P&D")
Celula.create(name: "Criação")
Celula.create(name: "Análise")