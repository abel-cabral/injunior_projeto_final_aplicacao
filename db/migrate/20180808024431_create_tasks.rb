class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.string :name
      t.references :goal, foreign_key: true
      t.boolean :done

      t.timestamps
    end
  end
end
