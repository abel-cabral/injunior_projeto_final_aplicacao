class CreateFusions < ActiveRecord::Migration[5.2]
  def change
    create_table :fusions do |t|
      t.references :user, foreign_key: true
      t.references :office, foreign_key: true
      t.boolean :status

      t.timestamps
    end
  end
end
