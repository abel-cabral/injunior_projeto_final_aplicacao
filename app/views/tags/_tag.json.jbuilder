json.extract! tag, :id, :identification, :name, :created_at, :updated_at
json.url tag_url(tag, format: :json)
