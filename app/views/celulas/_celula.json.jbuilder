json.extract! celula, :id, :name, :user_id, :created_at, :updated_at
json.url celula_url(celula, format: :json)
