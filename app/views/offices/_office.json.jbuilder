json.extract! office, :id, :name, :user_id, :created_at, :updated_at
json.url office_url(office, format: :json)
