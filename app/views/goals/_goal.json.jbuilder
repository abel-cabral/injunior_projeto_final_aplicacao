json.extract! goal, :id, :name, :celula_id, :office_id, :created_at, :updated_at
json.url goal_url(goal, format: :json)
