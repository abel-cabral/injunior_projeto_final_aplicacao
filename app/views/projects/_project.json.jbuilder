json.extract! project, :id, :name, :price, :description, :status, :technology_id, :created_at, :updated_at
json.url project_url(project, format: :json)
