class UsersController < ApplicationController
    def index
        @users=User.all
    end

    def show
        @user=User.find(params[:id])
        @fusion=Fusion.find_by(user_id: @user.id, status: nil)
        @office=Office.find_by(user_id: current_user.id, name: "Gestão de Pessoas")
        @teams=Team.where(user_id: @user.id)
        @projects=[]
        Project.where(project_id: @teams.project_id).each do |p|
            @projects.push(p)
        end
    end

end
