class Goal < ApplicationRecord
  belongs_to :celula, optional: true
  belongs_to :office, optional: true
end
