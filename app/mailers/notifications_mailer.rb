class NotificationsMailer < ApplicationMailer
    default from: "no-reply@exemplo.com"

    def article_notification(user,article)
        @user=user
        @article=article
        mail to: user.email, subject: "Novo Artigo!"
    end
end
